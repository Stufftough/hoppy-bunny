﻿/* GAME IDEAS
 * &
 * PROJECT TO-DOS
 * 
 * 
 * 
 * // *** GAME LEVEL SCENE *** //
 * 
 * // Gameplay
 *      limit hops, make gopher holes relevant
 * 
 * // UI
 *      improve timing for end menu
 *      add bouncing to GameOver txt
 *      check for medal on game end, if so display medal for achieved score tier
 *      share ui button functionality
 * 
 * // Sound
 * 
 * // Art
 * 
 * // *** MAIN MENU SCENE *** //
 * 
 * // UI
 *      research canvas scaler (different screen sizes)
 *      hoppy bunny text (bounces up and down)
 *      hoppy bunny player icon (bounces up and down)
 *      rate ui button functionality
 *      score ui button functionality/menu
 * 
 * // Sound
 * 
 * // Art
 * 
 * // *** PREPARE FOR PUBLISHING (Android, iOS) *** //
 *      setup mobile controls
 *      research how to link ui button to rate game
 *      research how to link ui button to share score
 * 
 * 
 * 
 * // Possible Improvements
 *      ---platform.position.y on creation according to camera bounds,
 *          because of differing screen sizes
 *      ---Extend triggers off screen,
 *          so player cannot bypass obstacles
 *          
 *          
 *          
 *          
 *          
 *          
 *  // Animate 'Tally Score' for Game End Menu (0 to 1 * highScore)
 *  // Have Animator send notifications to script for timing the Game End Menu
 *  
 *  // Tally Score
 *  // Enable Buttons
 *  // ^^^ Same Coroutine
 */