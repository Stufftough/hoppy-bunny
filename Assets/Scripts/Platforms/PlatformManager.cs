﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlatformManager : MonoBehaviour {

    GameManager _gm;

    public enum PlatformType {
        GenericPlatform,
        PipePlatform,
        GopherHolePlatform
    }

    // Platform prefabs
    public GenericPlatform prefabGenericPlatform;
    public PipePlatform prefabPipePlatform;
    public GopherHolePlatform prefabGopherHolePlatform;

    // ObjectPool List
    public List<PlatformPool> pools;

    public enum GeneratorMode {
        Default,        // Random obstacles, gopher hole always followed by pipe
        OnlyPlatforms,
        OnlyPipes,
        RandomObstacles
    }

    // Set platform generation pattern
    public GeneratorMode generatorMode = GeneratorMode.Default;
    IEnumerator<PlatformType?> generator;

    // Furthest point of current instantiated surface
    float _progressionOffset = 0;

    // Camera bounds, needed to determine object pools' Fill() count
    Camera _camera;
    Bounds _cameraBounds {
        get {
            if (_camera == null) return new Bounds(Vector3.zero, Vector3.zero);
            float height = _camera.orthographicSize * 2f;
            float width = _camera.aspect * height;
            return new Bounds(_camera.transform.position, new Vector3(width, height, 100f));
        }
    }

    // Max # of platforms in be generated per frame
    public int maxPlatforms = 100;

    void Awake() {
        _gm = FindObjectOfType<GameManager>();

        // Assign platform prefabs
        if (prefabGenericPlatform == null) prefabGenericPlatform = Resources.Load<GenericPlatform>("Platforms/Platform");
        if (prefabPipePlatform == null) prefabPipePlatform = Resources.Load<PipePlatform>("Platforms/Platform (pipe)");
        if (prefabGopherHolePlatform == null) prefabGopherHolePlatform = Resources.Load<GopherHolePlatform>("Platforms/Platform (gopher hole)");

        // Create object pools and .Add() them to 'pools' list 
        pools.Add(new PlatformPool(transform, PlatformType.GenericPlatform, prefabGenericPlatform));
        pools.Add(new PlatformPool(transform, PlatformType.PipePlatform, prefabPipePlatform));
        pools.Add(new PlatformPool(transform, PlatformType.GopherHolePlatform, prefabGopherHolePlatform));

        // Calculate camera.main bounds
        _camera = Camera.main;

        // Set progressionOffset for next platform's positioning
        _progressionOffset = (_camera.aspect * _camera.orthographicSize * -1f) + _camera.transform.position.x;

        // Fill object pools
        foreach (PlatformPool pool in pools) {
            pool.Fill(_cameraBounds);
        }

        generator = PlatformGenerator();
        StartCoroutine(PlatformGeneratorCoroutine());
    }

    public IEnumerator<PlatformType?> PlatformGenerator() {
        while (true) {
            switch (generatorMode) {
                case GeneratorMode.Default:
                    int rand = Random.Range(0, 2);
                    if(rand == 0) {
                        yield return PlatformType.PipePlatform;
                    } else if (rand == 1) {
                        yield return PlatformType.GopherHolePlatform;
                        yield return PlatformType.PipePlatform;
                    }
                    break;
                case GeneratorMode.OnlyPlatforms:
                    yield return PlatformType.GenericPlatform;
                    break;
                case GeneratorMode.OnlyPipes:
                    yield return PlatformType.PipePlatform;
                    break;
                case GeneratorMode.RandomObstacles:
                    yield return Random.Range(0, 2) == 0 ? PlatformType.PipePlatform : PlatformType.GopherHolePlatform;
                    break;
            }
            yield return null;
        }
    }

    IEnumerator PlatformGeneratorCoroutine() {
        while (true) {
            ReleasePlatforms();
            bool waited = false;

            // Generate/Place platforms
            for(int i = 0; i < maxPlatforms; i++) {
                generator.MoveNext();
                if (!pools.Any(p => p.type == generator.Current)) continue;
                PlatformPool pool = pools.First(p => p.type == generator.Current);
                if (!pool.CanGetPlatform()) {
                    yield return new WaitUntil(() => {
                        ReleasePlatforms();
                        return pool.CanGetPlatform();
                    });
                    waited = true;
                }
                PlatformComponent platform = pool.GetPlatform();
                platform.transform.eulerAngles = Vector2.zero;
                platform.transform.localScale = Vector2.one;
                platform.transform.position = new Vector2(_progressionOffset + pool.bounds.extents.x - pool.bounds.center.x, _cameraBounds.min.y + platform.height);
                platform.gameObject.SetActive(true);
                platform.SendMessage("PlatformSetup", SendMessageOptions.DontRequireReceiver);
                _progressionOffset += pool.bounds.size.x;
                if (waited) break;
            }
            if (!waited)
                yield return new WaitForEndOfFrame();
        }
    }

    void ReleasePlatforms() {
        Bounds camBounds = _cameraBounds;
        foreach(var pool in pools) {
            foreach(var platform in pool.list) {
                if (platform.gameObject.activeInHierarchy && platform.transform.position.x < _camera.transform.position.x && !camBounds.Intersects(platform.CalculateBounds()))
                    pool.ReleasePlatform(platform);
            }
        }
    }
}