﻿/* REVIEWED & COMPLETED
 * 12/17/16
 */
 
using UnityEngine;

public class PlatformComponent : MonoBehaviour {

    public float height;
    public Bounds bounds {
        get {
            if (_bounds.size.x == 0) {
                _bounds = CalculateBounds();
            }
            return _bounds;
        }
    }

    SpriteRenderer _sprite;
    Bounds _bounds;
    
    void Awake () {
        _sprite = GetComponent<SpriteRenderer>();
        _bounds = CalculateBounds();

        // Height of parent/ground platform
        height = _sprite.bounds.extents.y;
    }

    public Bounds CalculateBounds() {
        var totalBounds = new Bounds(transform.position, Vector3.zero);

        // Extend totalBounds to encapsulate our SpriteRenderer.bounds
        if (_sprite != null) totalBounds.Encapsulate(_sprite.bounds);

        // Extend totalBounds to encapsulate our children's SpriteRenderer.bounds
        SpriteRenderer[] childrenSpriteRenderers = GetComponentsInChildren<SpriteRenderer>(false);
        foreach(SpriteRenderer childrenSpriteRenderer in childrenSpriteRenderers) {
            totalBounds.Encapsulate(childrenSpriteRenderer.bounds);
        }

        return totalBounds;
    }

    // DEBUGS our bounding box when selected in hierarchy:
    void OnDrawGizmosSelected() {
        Gizmos.color = new Color(.5f, 0f, .5f, .5f);
        var bounds = CalculateBounds();
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }
}