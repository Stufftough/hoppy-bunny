﻿/* REVIEWED & COMPLETE
 * 12/17/16
 */

using UnityEngine;

public class PipePlatform : PlatformComponent {
    // Platform w/ Pipe obstacle

    public float minY = 3f;
    public float maxY = 7.5f;

    Transform _pipe;

    // Triggered through SendMessage by PlatformManager in UsePlatform()
    void PlatformSetup() {
        // Find the 'Pipe' child in the PipePlatform prefab
        foreach (Transform child in transform) {
            if (child.name == "Pipe") {
                _pipe = child;
                break;
            }
        }
        
        // Set 'Pipe' y position between minY and maxY
        _pipe.localPosition = new Vector3(0, Random.Range(minY, maxY), 0);
    }
}
