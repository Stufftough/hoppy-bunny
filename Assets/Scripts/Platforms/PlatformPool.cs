﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class PlatformPool {

    public PlatformManager.PlatformType type;
    public PlatformComponent prefab;
    public List<PlatformComponent> list;
    public Bounds bounds;

    Transform transform;

    public PlatformPool( Transform manager, PlatformManager.PlatformType platformType, PlatformComponent platformPrefab ){
        GameObject go = new GameObject(platformType + "Pool");
        go.transform.parent = manager;

        this.type = platformType;
        // TODO: Check if prefab is null
        this.prefab = platformPrefab;
        this.list = new List<PlatformComponent>();
        this.bounds = prefab.CalculateBounds();
        this.transform = go.transform;
    }

    public void Fill(Bounds cameraBounds) {
        PlatformComponent clone = Instantiate();

        Bounds cloneBounds = clone.CalculateBounds();
        int count = Mathf.Clamp(Mathf.CeilToInt(cameraBounds.size.x / cloneBounds.size.x), 0, 12);

        for (int i = 0; i < count; i++) Instantiate();
    }

    // Instantiate and return a PlatformComponent
    PlatformComponent Instantiate() {
        PlatformComponent clone = GameObject.Instantiate<PlatformComponent>(prefab);
        clone.gameObject.SetActive(false);
        clone.transform.parent = transform;
        list.Add(clone);
        return clone;
    }

    public bool CanGetPlatform() {
        return list.Any( platform => !platform.gameObject.activeInHierarchy );
    }

    public PlatformComponent GetPlatform() {
        return list.First(p => !p.gameObject.activeInHierarchy);
    }

    public void ReleasePlatform(PlatformComponent platform) {
        platform.gameObject.SetActive(false);
    }
}