﻿/* REVIEWED & COMPLETED
 * 12/16/16
 */

using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

    // Singleton
    public static GameManager instance = null;

    // GUIManager reference
    GUIManager _guim;

    public CanvasGroup fader; //
    public int fadeSpeed = 1; //
    AsyncOperation async;

    // Game Level Variables
    PlayerController _player;
    PlatformManager _platformManager;
    AudioSource _scoreAudio;
    int _score = 0;
    bool _changedGenerator = false;
    
    void Awake() {
        // Check if instance already exists, if not set instance to 'this', if instance is not 'this' destory 'this'
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        // Sets this gameObject to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        // If no Highscore, set Highscore to 0
        if (!PlayerPrefs.HasKey("Highscore")) {
            PlayerPrefs.SetInt("Highscore", 0);
        }
    }
    
    void Start () {
        _guim = FindObjectOfType<GUIManager>();
        _scoreAudio = GetComponent<AudioSource>();
    }
    
    void Update () {
        // TODO: Improve GameManager Update() condition statement
	    if(SceneManager.GetActiveScene().name == "Game Level" && _player != null && _player._hasInput && !_changedGenerator) {
            if (_platformManager != null) _platformManager.generatorMode = PlatformManager.GeneratorMode.Default;
            // TODO: Hide instructions
            _changedGenerator = true;
        }
    }

    public void AddScore() {
        // TODO: Some times double triggered
        _score += 1;
        _guim.UpdateScore(_score);
        _scoreAudio.Play();
    }

    public void Lose() {
        // TODO: Fader flashes white, use GUI Fader

        bool newScore = false;
        if(_score > PlayerPrefs.GetInt("Highscore")) {
            PlayerPrefs.SetInt("Highscore", _score);
            newScore = true;
        }
        _guim.GameOver(_score, newScore);
    }



    // Scene Management
    public void PlayGame() { // Main Menu 'Start' Button
        StartCoroutine(LoadNextLevel("Game Level"));
    }

    public void Restart() { // Game End Menu 'Restart' Button
        StartCoroutine(LoadNextLevel("Main Menu"));
    }

    private IEnumerator LoadNextLevel(string name) {
        // Fade-in fader to alpha = 1
        while (fader.alpha < 0.9f) {
            fader.alpha = Mathf.Lerp(fader.alpha, 1, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        fader.alpha = 1;

        // Load scene
        async = SceneManager.LoadSceneAsync(name);

        // Wait until scene has loaded
        yield return new WaitUntil(() => async.isDone);

        if (name == "Game Level") GameLevelLoad();
        else if (name == "Main Menu") MainMenuLoad();

        // Fade-out fader to alpha = 0
        while (fader.alpha > 0.1f) {
            fader.alpha = Mathf.Lerp(fader.alpha, 0, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        fader.alpha = 0;
    }

    void GameLevelLoad() {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _platformManager = GameObject.FindObjectOfType<PlatformManager>();
        _changedGenerator = false;
        _guim.GameLevelLoad();
    }

    void MainMenuLoad() {
        _score = 0;
        _guim.MainMenuLoad();
    }



    // Advertisements
    public void ShowAd() {
        if (Advertisement.IsReady()) {
            Advertisement.Show();
        }
    }
}