﻿/* REVIEWED & COMPLETED
 * 12/16/16
 */

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class GUIManager : MonoBehaviour {

    // Singleton
    public static GUIManager instance = null;

    // GameManager reference
    GameManager _gm;

    // Main Menu UI
    public GameObject canvasMainMenu;
    //public GameObject openingTitle;
    public Button btnStart;
    //public Button btnRate;
    //public Button btnScore;

    // Game UI
    public GameObject canvasGameLevel;
    Text scoreText;

    // Game End UI
    public Animator gameEndMenuAnimator;
    public Text curScoreNum;
    public Text highScoreNum;
    public GameObject newHighScoreRed;
    public GameObject btnsGameOver;
    public Button btnOK;

    void Awake() {
        // Check if instance already exists, if not set instance to 'this', if instance is not 'this' destory 'this'
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        // Sets this gameObject to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    
    void Start () {
        _gm = FindObjectOfType<GameManager>();

        // Main Menu Setup
        //btnStart = GameObject.FindGameObjectWithTag("btnStart").GetComponent<Button>();
        btnStart.onClick.AddListener(_gm.PlayGame);
        //btnStart = GameObject.FindGameObjectWithTag("btnRate").GetComponent<Button>();
        //btnStart.onClick.AddListener(_gm.XXX);
        //btnStart = GameObject.FindGameObjectWithTag("btnScore").GetComponent<Button>();
        //btnStart.onClick.AddListener(_gm.XXX);

        // Game Level Setup
        scoreText = canvasGameLevel.transform.Find("TXT - Score").gameObject.GetComponent<Text>();

        //curScoreNum = canvasGameLevel.transform.Find("Game End UI/Menu/TXT# - CurScore").gameObject.GetComponent<Text>();
        //highScoreNum = canvasGameLevel.transform.Find("Game End UI/Menu/TXT# - HighScore").gameObject.GetComponent<Text>();
        //newHighScoreRed = canvasGameLevel.transform.Find("Game End UI/Menu/New Highscore").gameObject;
        //btnsGameOver = canvasGameLevel.transform.Find("Game End UI/Buttons").gameObject;

        //btnOK = canvasGameLevel.transform.Find("Game End UI/Buttons/Button - OK").gameObject.GetComponent<Button>(); // buttons start deactivated
        btnOK.onClick.AddListener(_gm.Restart);
        //btnShare = canvasGameLevel.transform.Find("Game End UI/Buttons/Button - Share").gameObject.GetComponent<Button>(); // buttons start deactivated
        //btnShare.onClick.AddListener(_gm.XXX);
    }

    public void UpdateScore(int score) {
        scoreText.text = score.ToString();
    }

    public void GameOver(int score, bool newScore) {
        // At 0.0f, Deactivate score UI
        scoreText.gameObject.SetActive(false);
        highScoreNum.GetComponent<Text>().text = PlayerPrefs.GetInt("Highscore").ToString();
        if (newScore) {
            newHighScoreRed.SetActive(true);
        }
        // TODO: Remove pause btn

        // Activate 'gameOverEvent' animation and sfx
        gameEndMenuAnimator.gameObject.SetActive(true);
        gameEndMenuAnimator.SetBool("gameOverEvent", true);

        // At 1.8f, Tally curScoreNum from 0 to scoreText and completionCallback to Enable buttons
        StartCoroutine( GameOverTally(curScoreNum, score, .5f, 1.8f, () => btnsGameOver.SetActive(true)) );

        // Show Banner Ad at bottom of screen
        //_gm.ShowAd();
    }

    IEnumerator GameOverTally(Text ourNum, int total, float duration, float delay, Action completionCallback) {
        float totalTime = 0;
        float curDelayTime = 0;

        // Wait for delay
        while (curDelayTime < delay) {
            curDelayTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // Tally from 0 to 'total' for 'duration'
        while (totalTime <= duration) {
            totalTime += Time.deltaTime;
            ourNum.text = Mathf.CeilToInt(Mathf.Lerp(0, (float)total, totalTime / duration)).ToString();
            yield return new WaitForEndOfFrame();
        }
        ourNum.text = total.ToString();

        // Complete callback (enable buttons)
        completionCallback();
        yield break;
    }

    public void MainMenuLoad() {
        btnsGameOver.SetActive(false);

        canvasGameLevel.SetActive(false);
        canvasMainMenu.SetActive(true);

        // Show Banner Ad at top of screen
        //_gm.ShowAd();
    }

    public void GameLevelLoad() {
        canvasMainMenu.SetActive(false);
        canvasGameLevel.SetActive(true);

        scoreText.gameObject.SetActive(true);
        scoreText.text = "0";
        curScoreNum.text = "0";
        newHighScoreRed.gameObject.SetActive(false);

        // TODO: Add instruction icons
    }
}