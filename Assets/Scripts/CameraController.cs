﻿/* REVIEWED & COMPLETE
 * 12/17/16
 */

using UnityEngine;

public class CameraController : MonoBehaviour {

    private Transform _player;
    private float _offset;
    
	void Start () {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _offset = (Camera.main.aspect * Camera.main.orthographicSize) * 0.5f;        
    }
	
	void LateUpdate () {
        transform.position = new Vector3 (_player.position.x + _offset, 0, -10);
	}
}
