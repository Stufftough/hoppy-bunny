using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Scryptonite {
	public class PlatformManager : MonoBehaviour {
		[System.Serializable]
		public class PlatformManagerError : System.Exception {
			public PlatformManagerError() { }
			public PlatformManagerError(string message) : base(message) { }
			public PlatformManagerError(string message, System.Exception inner) : base(message, inner) { }
			protected PlatformManagerError(
			  System.Runtime.Serialization.SerializationInfo info,
			  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
		}

		public enum GeneratorMode {
			Default,
			DefaultAfterFirstJump
		}

		public enum PlatformType {
			GroundPlatform,
			PipePlatform,
			GopherHolePlatform
		}

		public BunnyController bunny;
		public new Camera camera;
		private Bounds cameraBounds {
			get {
				if (camera == null) return new Bounds(Vector3.zero, Vector3.zero);
				float height = camera.orthographicSize * 2f;
				float width = camera.aspect * height;
				return new Bounds(camera.transform.position, new Vector3(width, height, 100f));
			}
		}

		public GeneratorMode mode = GeneratorMode.Default;
		public float platformProgressionOffset;
		private IEnumerator<PlatformType?> generator;

		public GroundPlatform groundPlatformPrefab;
		public PipePlatform pipePlatformPrefab;
		public GopherHolePlatform gopherHolePlatformPrefab;

		public List<PlatformPool> pools;

		void Awake() {
			if (bunny == null) bunny = GameObject.FindObjectOfType<BunnyController>();
			if (camera == null) camera = Camera.main;
			if (groundPlatformPrefab == null) groundPlatformPrefab = Resources.Load<GroundPlatform>("Prefabs/Ground Platform");
			if (pipePlatformPrefab == null) pipePlatformPrefab = Resources.Load<PipePlatform>("Prefabs/Pipe Platform");
			if (gopherHolePlatformPrefab == null) gopherHolePlatformPrefab = Resources.Load<GopherHolePlatform>("Prefabs/Gopher Hole Platform");

			pools.Add(new PlatformPool(transform, PlatformType.GroundPlatform, groundPlatformPrefab));
			pools.Add(new PlatformPool(transform, PlatformType.PipePlatform, pipePlatformPrefab));
			pools.Add(new PlatformPool(transform, PlatformType.GopherHolePlatform, gopherHolePlatformPrefab));

			if (camera != null) {
				Bounds camBounds = cameraBounds;
				foreach (PlatformPool pool in pools) { 
					pool.Fill(camBounds);
				}
				platformProgressionOffset = camera.transform.position.x - cameraBounds.extents.x;
			} else {
				Debug.LogWarningFormat("[{0}] Unable to fill platform pool without a camera", gameObject.name);
			}

			if(bunny == null && mode == GeneratorMode.DefaultAfterFirstJump) {
				Debug.LogWarningFormat("[{0}] Unable to find the bunny, changing generator mode from DefaultAfterFirstJump to Default", gameObject.name);
				mode = GeneratorMode.Default;
			}

			generator = PlatformGenerator();
			StartCoroutine(PlatformGeneratorCoroutine());
		}

		IEnumerator<PlatformType?> PlatformGenerator() {
			while (true) {
				switch (mode) {
					case GeneratorMode.Default:
						yield return PlatformType.GroundPlatform;
						yield return Random.Range(0, 2) == 0 ? PlatformType.GopherHolePlatform : PlatformType.PipePlatform;
						break;
					case GeneratorMode.DefaultAfterFirstJump:
						if (bunny.totalJumps >= 1) {
							mode = GeneratorMode.Default;
							continue;
						}
						yield return PlatformType.GroundPlatform;
						break;
				}
				yield return null;
			}
		}

		IEnumerator PlatformGeneratorCoroutine() {
			while (true) {
				ReleasePlatforms();
				bool waited = false;
				for (int i = 0; i < 100; i++) {
					generator.MoveNext();
					if (!pools.Any(p => p.type == generator.Current)) continue;
					PlatformPool pool = pools.First(p => p.type == generator.Current);
					if (!pool.CanGetPlatform()) {
						//Debug.LogFormat("Waiting to create \"{0}\"", generator.Current);
						yield return new WaitUntil(() => {
							ReleasePlatforms();
							return pool.CanGetPlatform();
						});
						waited = true;
					}
					PlatformComponent platform = pool.GetPlatform();
					platform.transform.localEulerAngles = Vector3.zero;
					platform.transform.localScale = Vector3.one;
					platform.transform.position = new Vector3(platformProgressionOffset + pool.bounds.extents.x - pool.bounds.center.x, -4f);
					platform.SendMessage("PlatformSetup", SendMessageOptions.DontRequireReceiver);
					platformProgressionOffset += pool.bounds.size.x;
					if (waited) break;
				}
				if (!waited)
					yield return new WaitForEndOfFrame();
			}
		}

		void ReleasePlatforms() {
			Bounds cameraBounds = this.cameraBounds;
			foreach (PlatformPool pool in pools) {
				foreach (PlatformComponent platform in pool.list) {
					if (platform.transform.position.x < camera.transform.position.x && !cameraBounds.Intersects(platform.CalculateBounds())) {
						pool.ReleasePlatform(platform);
					}
				}
			}
		}
	}
}
