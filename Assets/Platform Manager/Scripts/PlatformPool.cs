using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Scryptonite {

	[System.Serializable]
	public class PlatformPool {

		public PlatformManager.PlatformType type;
		private Transform transform;
		public PlatformComponent prefab;
		public List<PlatformComponent> list;
		public Bounds bounds;

		public PlatformPool(Transform manager, PlatformManager.PlatformType platformType, PlatformComponent platformPrefab) {
			GameObject go = new GameObject(platformType + " Pool");
			go.transform.parent = manager;

			this.type = platformType;
			this.transform = go.transform;
			this.prefab = platformPrefab;
			if (prefab == null)
				throw new PlatformManager.PlatformManagerError(string.Format("[{0}] platformPrefab should not be null", transform.gameObject.name));
			if (!IsPrefab(platformPrefab))
				throw new PlatformManager.PlatformManagerError(string.Format("[{0}] platformPrefab should be a reference to a .prefab in your Assets/", transform.gameObject.name));
			this.bounds = prefab.CalculateBounds();
			this.list = new List<PlatformComponent>();
		}

		public void Fill(Bounds cameraBounds) {
			PlatformComponent clone = Instantiate();
			Bounds bounds = clone.CalculateBounds();
			if (this.bounds.size.x == 0) {
				this.bounds = bounds;
			}
			int count = Mathf.Clamp(Mathf.CeilToInt(cameraBounds.size.x / bounds.size.x), 0, 12);
			for (int i = 0; i < count; i++)
				Instantiate();
		}

		private PlatformComponent Instantiate() {
			PlatformComponent clone = GameObject.Instantiate<PlatformComponent>(prefab);
			clone.gameObject.SetActive(false);
			clone.transform.parent = transform;
			list.Add(clone);
			return clone;
		}

		public bool CanGetPlatform() {
			return list.Any(platform => !platform.gameObject.activeInHierarchy);
		}

		public PlatformComponent GetPlatform() {
			PlatformComponent platform = list.First(p => !p.gameObject.activeSelf);
			if (platform == null)
				throw new PlatformManager.PlatformManagerError(string.Format("[{0}] There are no free platforms to get. You should check CanGetPlatform() before using GetPlatform()", transform.gameObject.name));
			platform.gameObject.SetActive(true);
			return platform;
		}



		public void ReleasePlatform(PlatformComponent platform) {
			if (!list.Contains(platform))
				throw new PlatformManager.PlatformManagerError(string.Format("[{0}] The platform \"{1}\" is not in this pool, so it cannot be released", transform.gameObject.name, platform));
			platform.gameObject.SetActive(false);
			platform.transform.parent = transform;
		}




		public static bool IsPrefab(GameObject possiblePrefab) {
			return possiblePrefab.scene.rootCount == 0;
		}

		public static bool IsPrefab(Component possiblePrefabComponent) {
			return IsPrefab(possiblePrefabComponent.gameObject);
		}

	}
}
