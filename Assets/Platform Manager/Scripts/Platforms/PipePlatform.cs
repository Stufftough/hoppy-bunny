using UnityEngine;

namespace Scryptonite {
	public class PipePlatform : PlatformComponent {
		Transform _pipe;
		Transform pipe {
			get {
				if (_pipe == null) {
					foreach (Transform child in transform) {
						if (child.gameObject.name == "Pipe"
						|| child.GetComponent<PipePlatformPipe>() != null) {
							_pipe = child;
							break;
						}
					}
				}
				return _pipe;
			}
		}
		public new void Awake() {
			base.Awake(); // make sure PlatformComponent does its bounds calculations
			_pipe = pipe;
		}
		void PlatformSetup() {
			if (_pipe == null) _pipe = pipe;
			if (pipe == null) {
				Debug.LogWarningFormat("[{0}] Unable to find 'Pipe' child!", gameObject.name);
				return;
			}
			pipe.localPosition = new Vector3(0, Random.Range(4.15f, 8.5f), 0);
			SpriteRenderer[] spriteRenderers = pipe.GetComponentsInChildren<SpriteRenderer>();
			foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
				spriteRenderer.color = Color.white;
			}
		}
	}
}
