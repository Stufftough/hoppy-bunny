using UnityEngine;

namespace Scryptonite {
	public class PipePlatformPipe : MonoBehaviour {
		void OnCollisionEnter2D(Collision2D collision) {
			BunnyController bunny = collision.gameObject.GetComponent<BunnyController>();
			if (bunny != null) {
				SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
				foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
					spriteRenderer.color = Color.red;
				}
			}
		}
	}
}
