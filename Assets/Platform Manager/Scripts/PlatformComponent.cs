using UnityEngine;
using System.Collections.Generic;

namespace Scryptonite {
	public class PlatformComponent : MonoBehaviour {
		private Bounds _bounds;
		public Bounds bounds {
			get {
				if (_bounds.size.x == 0) {
					CalculateBounds();
				}
				return _bounds;
			}
		}
		public void Awake() {
			CalculateBounds();
		}
		public Bounds CalculateBounds() {
			Bounds bounds = new Bounds(transform.position, Vector3.zero);
			List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>(GetComponents<SpriteRenderer>());
			spriteRenderers.AddRange(GetComponentsInChildren<SpriteRenderer>());
			foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
				if (!spriteRenderer.gameObject.activeInHierarchy) continue;
				bounds.Encapsulate(spriteRenderer.bounds);
			}
			if ((_bounds.size.x == 0 || _bounds.size.x != bounds.size.x) && bounds.size.x != 0) _bounds = bounds;
			// TODO: Hunt down why bounds' size.x would sometimes be 0 when CalculateBounds is called multiple times
			if (bounds.size.x == 0 && _bounds.size.x != 0) bounds = _bounds;
			return bounds;
		}
		void OnDrawGizmosSelected() {
			Gizmos.color = new Color(.5f, 0f, .5f, .5f);
			Bounds bounds = CalculateBounds();
			Gizmos.DrawWireCube(bounds.center, bounds.size);
		}
	}
}
