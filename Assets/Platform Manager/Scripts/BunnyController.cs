using UnityEngine;

namespace Scryptonite { // namespaced to avoid class naming conflicts
	public class BunnyController : MonoBehaviour {
		public float moveSpeed = 4f;
		public float jumpSpeed = 12f;
		public float rotationFactor = 3f;
		public bool limitedJumps = false;
		public int maxJumps = 3;
		private int jumps;
		public int totalJumps { get; private set; }
		public LayerMask jumpResetLayers = 0;
		
		// this script can move the camera, for debugging simplicity:
		public bool shouldMoveCamera = true;

		private Rigidbody2D rb;

		private bool _prevShouldJump;
		private bool _shouldJump;
		private bool doJump;
		
		void Awake() {
			rb = GetComponent<Rigidbody2D>();
			totalJumps = 0;

			if (jumpResetLayers == 0) {
				Debug.LogWarning("Just a warning, but Jump Reset Layers should be configured to something other than \"Nothing\". Reconfiguring it as \"Ground\".");
				jumpResetLayers = 1 << LayerMask.NameToLayer("Ground");
			}

			LateUpdate();
		}

		void Update() {
			// if the user is holding space and wasn't holding space in the previous Update(), perform jump in next FixedUpdate()
			_prevShouldJump = _shouldJump;
			_shouldJump = (Input.GetKeyDown("space") || Input.GetMouseButtonDown(0)) && !_prevShouldJump;
			if (_shouldJump) doJump = true;
		}

		void OnCollisionEnter2D(Collision2D collision) {
			if (((1 << collision.gameObject.layer) & jumpResetLayers) != 0) {
				//Debug.Log("Resetting jump counter!");
				jumps = maxJumps;
			}
		}

		void FixedUpdate() {
			Vector2 velocity = rb.velocity;
			velocity.x = Mathf.Clamp(moveSpeed, 0, Mathf.Infinity);
			if (doJump) {
				if (!limitedJumps || jumps-- > 0) {
					totalJumps++;
					velocity.y = jumpSpeed;
				}
				doJump = false; // don't perform another jump in next FixedUpdate() unless Update() says so
			}
			rb.velocity = velocity;

			rb.MoveRotation(Mathf.LerpAngle(rb.rotation, Mathf.Clamp(rb.velocity.y * rotationFactor, -90f, 90f), .5f));
		}

		void LateUpdate() {
			if (shouldMoveCamera) {
				float offset = Camera.main.aspect * Camera.main.orthographicSize * -.5f;
				Transform camera = Camera.main.transform;
				camera.position = new Vector3(
					transform.position.x - offset,
					camera.position.y,
					camera.position.z
				);
			}
		}
	}
}
