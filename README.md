Hoppy Bunny
===========
<sup>*teamTALIMA Project #1 &mdash; [Developed live on twitch.tv/teamTALIMA](https://www.twitch.tv/teamtalima)*</sup>

A 2D mobile platform game based on Flappy Bird. :rabbit:

An Android build of the game will be available soon.


## Specs

- Unity `5.5.0f3`
- Programmed in C#


## Needed Contributions

Feel free to submit a merge request!

- *Sound Effects*
    - [ ] Death by running into obstacle
    - [ ] Score increment
    - [ ] Bunny hop


## Talima Todo
- [ ] Game build for Android 
- [ ] Finishing UI animations and SFX
